import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    
    const currentUser = ref<User>({
        id:1,
        email: 'me.1@gmail.com',
        password: "password@1234",
        fullName: 'Bob Me',
        gender: 'male',
        roles: ['user']
    })
    return{ currentUser }
})